from django.conf.urls import url
from pages import views

urlpatterns = [
    url(r'^about/$', views.AboutView.as_view(), name='about'),
    url(r'^faq/$', views.FAQView.as_view(), name='faq'),
    url(r'^how/$', views.HowView.as_view(), name='how'),
    url(r'^privacy/$', views.PrivacyView.as_view(), name='privacy'),
    url(r'^terms/$', views.TermsView.as_view(), name='terms'),
    url(r'^thankyou/$', views.ThankyouView.as_view(), name='thankyou'),
]
