from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse


class PagesStaticViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'weekly'

    def items(self):
        return ['about', 'faq', 'how', 'privacy', 'terms']

    def location(self, item):
        return reverse(item)
