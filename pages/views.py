from django.views.generic import TemplateView


class AboutView(TemplateView):
    template_name = "pages/about.html"


class FAQView(TemplateView):
    template_name = "pages/faq.html"


class HowView(TemplateView):
    template_name = "pages/how.html"


class PrivacyView(TemplateView):
    template_name = "pages/privacy.html"


class TermsView(TemplateView):
    template_name = "pages/terms.html"


class ThankyouView(TemplateView):
    template_name = "pages/thankyou.html"
