from django.contrib.sitemaps import Sitemap
from parcels.models import Parcel


class ParcelSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.7

    def items(self):
        return Parcel.objects.all()

    def lastmod(self, obj):
        return obj.modified_date
