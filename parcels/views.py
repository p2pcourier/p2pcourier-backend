from django.views import generic
from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import Http404
from parcels.models import Parcel
from parcels.forms import ParcelForm, ParcelListForm
from django_messages.forms import ComposeForm
from parcels.tasks import inform_travellers_task
from django.views.generic.edit import FormView
from django.utils.translation import ugettext as _


class ParcelList(generic.ListView, FormView):
    """
    List view of all available requests
    """
    template_name = 'parcels/parcel_list.html'
    queryset = Parcel.objects.all().order_by('-delivery_date')
    form_class = ParcelListForm

class ParcelDetail(generic.DetailView):
    """
    Parcel detail view for a single post and apply
    TODO: Currently recipient and subject are just hidden, we need to modify
    forms to ignore changes coming from post and use details we want, i.e.
    someone can "unhide" recipient and subject and send whatever they want
    """
    template_name = 'parcels/parcel_detail.html'
    model = Parcel
    queryset = Parcel.objects.all()

    def get_context_data(self, **kwargs):
        context = super(ParcelDetail, self).get_context_data(**kwargs)
        parcel = self.get_object()
        context['form'] = ComposeForm()
        context['subject'] = \
            _('Request to deliver a package from {} to {}').format(
                parcel.origin.name_ascii, parcel.destination.name_ascii)
        context['message_placeholder'] = \
            _('Please describe your travel details here.')
        return context


class ParcelCreate(generic.CreateView):
    """
    Parcel create view
    """
    template_name = 'parcels/parcel_form.html'
    model = Parcel
    form_class = ParcelForm

    def form_valid(self, form):
        form.instance.requester = self.request.user
        return super(ParcelCreate, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, _('Request successfully created'))
        inform_travellers_task.delay(self.object.id)
        return reverse('parcel-detail',
                       args=(self.object.id, self.object.slug))


class ParcelUpdate(generic.UpdateView):
    """
    Parcel update view
    """
    template_name = 'parcels/parcel_form.html'
    model = Parcel
    form_class = ParcelForm

    def get_success_url(self):
        messages.success(self.request, _('Request successfully updated'))
        inform_travellers_task.delay(self.object.id)
        return reverse('parcel-detail',
                       args=(self.object.id, self.object.slug))


class ParcelDelete(generic.DeleteView):
    """
    Parcel delete view
    """
    model = Parcel

    def get_success_url(self):
        messages.success(self.request, _('Request successfully deleted'))
        return reverse_lazy('parcels')

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user """
        parcel_to_delete = super(ParcelDelete, self).get_object()
        if not parcel_to_delete.requester == self.request.user:
            raise Http404
        return parcel_to_delete
