from django.test import TestCase, override_settings
from django.contrib.auth import get_user_model

from parcels.models import Parcel
from cities_light.models import Country, City

import datetime
from django.utils import timezone


@override_settings(CELERY_ALWAYS_EAGER=True)
class ParcelTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        This class method sets up test databse for the below tests
        """
        cls.country = Country.objects.create(name='Sibir')

        cls.sender = get_user_model().objects.create_user(
            "free", "proj@mayhem.com", "hitme")

        cls.city_org = City.objects.create(
            name="Langepas", country=cls.country)

        cls.city_des = City.objects.create(
            name="Megion", country=cls.country)

        cls.today = timezone.now().date()

        cls.parcel = Parcel.objects.create(
            requester=cls.sender,
            origin=cls.city_org,
            destination=cls.city_des,
            delivery_date=cls.today+datetime.timedelta(weeks=2),
            notes="soap",
            size="medium",
            flexible="no")



    def test_parcels_list(self):
        """
        Tests list of parcels
        """
        response = self.client.get('/requests/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "parcels/parcel_list.html")
        self.assertContains(response, "Sibir")
        self.assertContains(response, "Megion")
        self.assertFalse(len(response.context["parcel_list"]) == 4)


    def test_parcel_detail(self):
        """
        Tests detail view of a parcel
        """
        log = self.client.login(username="free", password="hitme")
        response = self.client.get('/requests/1/langepas-megion/')
        self.assertContains(response, 'Megion')
        self.assertNotContains(response, "London")
        self.assertTemplateUsed(response, "parcels/parcel_detail.html")
        logo = self.client.logout()
        self.assertEqual(logo, None)


    def test_parcel_create(self):
        """
        Tests creation of a new parcel
        """
        log_fail = self.client.login(username="dostoynoy",
                                     password="president")
        self.assertFalse(log_fail)

        log = self.client.login(username="free", password="hitme")
        self.assertEqual(log, True)

        response = self.client.post(
            "/requests/new",
                {
                    "origin":self.city_des.id,
                    "destination":self.city_org.id,
                    "delivery_date":self.today + datetime.timedelta(days=5),
                    "notes":"dassax",
                    "size":"large",
                    "weight":"medium",
                    "tips":True
                },
            follow=True)
        self.assertContains(response, "Langepas")
        logo = self.client.logout()
        self.assertEqual(logo, None)



    def test_parcel_create_fail(self):
        """
        Tests raising exceptions if form contains errors
        """
        log = self.client.login(username="free", password="hitme")
        response = self.client.post("/requests/new",
            {
                "origin":111111111111111,
                "destination":111111111111111,
                "delivery_date":111111111111111,
                "size":"dostoyniy",
                "weight":"dostoyniy",
            },
            follow=True)


        self.assertFormError(response, 'form', None, None)

        self.assertFormError(response, 'form', 'origin',
            ('Select a valid choice. That choice is not one of the available '
             'choices.'))

        self.assertFormError(response, 'form', 'destination',
            ('Select a valid choice. That choice is not one of the available '
             'choices.'))

        self.assertFormError(response, 'form', 'delivery_date',
            ("Enter a valid date."))

        self.assertFormError(response, 'form', 'size',
            ('Select a valid choice. dostoyniy is not one of the'
             ' available choices.'))

        self.assertFormError(response, 'form', 'weight',
            ('Select a valid choice. dostoyniy is not one of the'
             ' available choices.'))



    def test_parcel_update(self):
        """
        Tests parcel update.
        """
        login = self.client.login(username='free', password='hitme')
        self.assertTrue(login)

        _id = self.parcel.id
        _slug = self.parcel.slug

        response = self.client.post("/requests/update/{}/{}/".format(_id,
                _slug),
            {
                "origin":self.city_des.id,
                "destination":self.city_org.id,
                "delivery_date":self.today + datetime.timedelta(days=5),
                "notes":"prez",
                "size":"small",
                "weight":"large",
                "tips":False
            },
            follow=True)

        self.assertContains(response, 'prez')
        self.assertNotContains(response, 'dassax')
        self.assertTemplateUsed(response, 'parcels/parcel_form.html')
        self.client.logout()


    def test_parcel_delete(self):
        """
        Tests parcel deletion.
        """
        login = self.client.login(username='free', password='hitme')
        self.assertEqual(login, True)

        _id = self.parcel.id
        _slug = self.parcel.slug

        response = self.client.post('/requests/delete/{}/{}/'.format(_id,
            _slug), follow=True)

        self.assertContains(response, 'Request successfully deleted')
        self.assertTemplateUsed(response, 'parcels/parcel_list.html')
        self.assertTemplateNotUsed(response, 'parcels/parcel_detail.html')
        self.client.logout()


    def test_anonymous_parcel_create_fail(self):
        """
        Test to check if anonymous user redirected to login page
        when they try to create a parcel.
        """
        response = self.client.post('/requests/new',
            {
                "origin":self.city_des.id,
                "destination":self.city_org.id,
                "delivery_date":self.today + datetime.timedelta(days=5),
                "notes":"prez",
                "size":"small",
                "weight":"large",
                "tips":False
            },
            follow=True)

        self.assertRedirects(response, '/accounts/login/?next=/requests/new')


    def test_anonymous_parcel_update_fail(self):
        """
        Test to check if anonymous user redirected to login page
        when they try to update a parcel.
        """

        _id = self.parcel.id
        _slug = self.parcel.slug

        response = self.client.post('/requests/update/{}/{}/'.format(_id, _slug),
            {
                "origin":self.city_des.id,
                "destination":self.city_org.id,
                "delivery_date":self.today + datetime.timedelta(days=5),
                "notes":"prez",
                "size":"small",
                "weight":"large",
                "tips":False
            },
            follow=True)

        self.assertRedirects(response,
            '/accounts/login/?next=/requests/update/{}/{}/'.format(_id, _slug))
