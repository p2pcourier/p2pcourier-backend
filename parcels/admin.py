from django.contrib import admin
from parcels.models import Parcel


class ParcelAdmin(admin.ModelAdmin):
    list_display = ('requester',
                    'origin',
                    'destination',
                    'delivery_date',
                    'flexible',
                    'tips',
                    'slug')

    search_fields = ('requester__username',
                    'origin__name',
                    'destination__name')
    
    raw_id_fields = ('origin', 'destination')


admin.site.register(Parcel, ParcelAdmin)
