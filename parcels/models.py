from django.db import models
from home.models import BaseModel
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField


@python_2_unicode_compatible
class Parcel(BaseModel):
    """
    This model contains all the parcels
    """
    requester = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,)
    origin = models.ForeignKey('cities_light.City', null=True,
                               related_name='origin_parcel')
    destination = models.ForeignKey('cities_light.City', null=True,
                                    related_name='destination_parcel')
    delivery_date = models.DateField(_("Delivery date"),)
    notes = models.TextField(_("Details"), blank=True)
    slug = AutoSlugField(populate_from='get_slug_name', always_update=True)

    # Size choices
    SIZES = (
        ('small', _('Small (roughly A5-paper size parcel)')),
        ('medium', _('Medium (roughly A4-paper size parcel)')),
        ('large', _('Large (parcel bigger than A4-paper size)')),
        (None, _('Please pick a size')),
    )

    # Weight choices, should be localised depending on locale
    WEIGHTS = (
        ('light', _('Light (up to 0.5 kg)')),
        ('medium', _('Medium (up to 1 kg)')),
        ('heavy', _('Heavy (more than 1 kg)')),
        (None, _('Please pick a weight')),
    )


    size = models.CharField(max_length=10, choices=SIZES)

    weight = models.CharField(max_length=10, choices=WEIGHTS)

    flexible = models.BooleanField(_("Date flexible"), default=False)

    tips = models.BooleanField(_("Tips"), default=False)

    def past_request(self):
        now = timezone.now().date()
        return self.delivery_date < now

    def __str__(self):
        if self.requester.first_name:
            name = self.requester.first_name
        else:
            name = self.requester.username
        pattern = _(u"{} has a parcel for delivery from {}, {} to {}, {} by {}")
        return pattern.format(
            name, self.origin.name_ascii, self.origin.country.name_ascii,
            self.destination.name_ascii, self.destination.country.name_ascii,
            self.delivery_date.strftime('%d %B %Y'))


    def get_absolute_url(self):
        """
        Gets the url of the object to its detail view page
        """
        return reverse('parcel-detail', args=(str(self.id), self.slug))

    def get_slug_name(self):
        """
        Return a slug name for this model
        """
        return "{}-{}".format(self.origin.name_ascii.lower(),
            self.destination.name_ascii.lower())

    class Meta:
        verbose_name = _('request')
        verbose_name_plural = _('requests')
