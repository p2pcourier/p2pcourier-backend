import datetime
from haystack import indexes
from parcels.models import Parcel


class ParcelIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Parcel index with text field which defines all model fields to be indexed
    in a template and additional filters.
    """
    text = indexes.CharField(document=True, use_template=True)
    date_parcel = indexes.DateTimeField(model_attr='delivery_date')
    origin_parcel = indexes.CharField(model_attr='origin')
    destination_parcel = indexes.CharField(model_attr='destination')

    def get_model(self):
        return Parcel

    def __str__(self):
        return 'test'