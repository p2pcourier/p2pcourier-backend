from django.conf.urls import url
from django.contrib.auth.decorators import login_required as log
from parcels import views

urlpatterns = [
    url(r'^$', views.ParcelList.as_view(), name="parcels"),
    url(r'^(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$',
        log(views.ParcelDetail.as_view()), name="parcel-detail"),
    url(r'^new$', log(views.ParcelCreate.as_view()), 
        name="parcel-new"),
    url(r'^update/(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$',
        log(views.ParcelUpdate.as_view()), name="parcel-update"),
    url(r'^delete/(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$',
        log(views.ParcelDelete.as_view()), name="parcel-delete"),
]
