from django.conf import settings
from django.contrib.sites.models import Site
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from django.template import loader
from django.utils.translation import ugettext as _
from p2pcourier.celery import app
# from p2pcourier.settings import DEFAULT_FROM_EMAIL as from_email
from parcels.models import Parcel
from travels.models import Travel


'''
@app.task
def inform_travellers_periodic_task():
    """
    Checks the db and sends emails to travellers
    periodically
    """
    return "Dostoyniy prezident"
'''

@app.task
def inform_travellers_task(parcel_id):
    """
    Checks the db and sends emails to travellers
    when a request created/updated
    """
    # TODO I lost 3 to 4 hours on this..
    from_email = 'noreply@post4tips.com'

    parcel = Parcel.objects.get(pk=parcel_id)

    travels = Travel.objects.select_related().exclude(
        traveller_id=str(parcel.requester_id)).exclude(
        traveller__match_emails=False)

    travels = travels.filter(origin=parcel.origin).filter(
        destination=parcel.destination)

    travels = travels.filter(journey_end__lte=parcel.delivery_date)

    if travels:
        raw_emails = set(travels.values_list('traveller__email'))
        recipient_list = [email[0] for email in raw_emails]
    else:
        return

    subject = _("New request matching your travel")

    domain = Site.objects.get_current()

    html_file = "notifications/new_request.html"
    text_file = "notifications/new_request.txt"

    prefix = "http://"
    if 'prod' in settings.SETTINGS_MODULE:
        prefix = "https://"

    data = {
        "origin" : parcel.origin,
        "destination" : parcel.destination,
        "date" : parcel.delivery_date,
        "user" : parcel.requester,
        "id" : parcel.id,
        "slug" : parcel.slug,
        "site" : domain,
        "prefix" : prefix,
    }

    html_content = loader.get_template(html_file).render(data)
    text_content = loader.get_template(text_file).render(data)

    messages = []
    for recipient in recipient_list:
        message = EmailMultiAlternatives(subject, text_content, from_email,
            [recipient])
        message.attach_alternative(html_content, "text/html")
        messages.append(message)

    connection = mail.get_connection()
    connection.send_messages(messages)
