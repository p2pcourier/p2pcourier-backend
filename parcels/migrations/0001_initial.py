# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cities_light', '0005_auto_20160129_1651'),
    ]

    operations = [
        migrations.CreateModel(
            name='Parcel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('delivery_date', models.DateField(verbose_name=b'Delivery date', blank=True)),
                ('flexible', models.BooleanField(default=False, verbose_name=b'Delivery date flexible')),
                ('tips', models.BooleanField(default=True, verbose_name=b'Happy to tip')),
                ('max_size', models.CharField(max_length=10, choices=[(b'small', b'Small (A5 document, small bag or parcel)'), (b'medium', b'Medium (A4 document, medium bag or parcel)'), (b'large', b'Large'), (None, b'Please pick a size')])),
                ('max_weight', models.CharField(max_length=10, choices=[(b'light', b'Light (no more than 500 gr.)'), (b'medium', b'Medium (no more than 1 kg.)'), (b'heavy', b'Heavy'), (None, b'Please pick a weight')])),
                ('destination', models.ForeignKey(related_name='destination_parcel', to='cities_light.City', null=True)),
                ('origin', models.ForeignKey(related_name='origin_parcel', to='cities_light.City', null=True)),
                ('requester', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
