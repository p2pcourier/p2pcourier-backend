# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parcels', '0002_parcel_notes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='parcel',
            name='max_size',
        ),
        migrations.RemoveField(
            model_name='parcel',
            name='max_weight',
        ),
        migrations.AddField(
            model_name='parcel',
            name='size',
            field=models.CharField(blank=True, max_length=10, choices=[(b'small', b'Small (A5 document, small bag or parcel)'), (b'medium', b'Medium (A4 document, medium bag or parcel)'), (b'large', b'Large (more details in the "notes" section'), (None, b'Please pick a size')]),
        ),
        migrations.AddField(
            model_name='parcel',
            name='weight',
            field=models.CharField(blank=True, max_length=10, choices=[(b'light', b'Light (no more than 0.5 kg)'), (b'medium', b'Medium (no more than 1 kg)'), (b'heavy', b'Heavy (more than 1 kg)'), (None, b'Please pick a weight')]),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='delivery_date',
            field=models.DateField(verbose_name=b'Delivery date'),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='flexible',
            field=models.BooleanField(default=True, verbose_name=b'Delivery date flexible'),
        ),
    ]
