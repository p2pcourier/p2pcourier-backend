# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parcels', '0003_auto_20160212_0018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='size',
            field=models.CharField(max_length=10, choices=[(b'small', b'Small (A5 document, small bag or parcel)'), (b'medium', b'Medium (A4 document, medium bag or parcel)'), (b'large', b'Large (more details in the "notes" section'), (None, b'Please pick a size')]),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='weight',
            field=models.CharField(max_length=10, choices=[(b'light', b'Light (no more than 0.5 kg)'), (b'medium', b'Medium (no more than 1 kg)'), (b'heavy', b'Heavy (more than 1 kg)'), (None, b'Please pick a weight')]),
        ),
    ]
