# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parcels', '0004_auto_20160212_0020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='flexible',
            field=models.CharField(max_length=5, choices=[(b'Yes', b'Yes'), (b'No', b'No'), (None, b'Please choose an option')]),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='size',
            field=models.CharField(max_length=10, choices=[(b'small', b'Small(roughly A5-paper size parcel)'), (b'medium', b'Medium (roughly A4-paper size parcel)'), (b'large', b'Large (bigger parcel'), (None, b'Please pick a size')]),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='tips',
            field=models.CharField(max_length=5, choices=[(b'Yes', b'Yes'), (b'No', b'No'), (None, b'Please choose an option')]),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='weight',
            field=models.CharField(max_length=10, choices=[(b'light', b'Light(up to 0.5 kg)'), (b'medium', b'Medium(up to 1 kg)'), (b'heavy', b'Heavy(more than 1 kg)'), (None, b'Please pick a weight')]),
        ),
    ]
