# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parcels', '0007_auto_20160219_1014'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='parcel',
            options={'verbose_name_plural': 'requests'},
        ),
    ]
