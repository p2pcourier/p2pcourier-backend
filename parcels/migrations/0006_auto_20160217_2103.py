# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parcels', '0005_auto_20160217_2101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcel',
            name='size',
            field=models.CharField(max_length=10, choices=[(b'small', b'Small(roughly A5-paper size parcel)'), (b'medium', b'Medium(roughly A4-paper size parcel)'), (b'large', b'Large(bigger parcel'), (None, b'Please pick a size')]),
        ),
    ]
