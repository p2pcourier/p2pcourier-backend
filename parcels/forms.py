import autocomplete_light.shortcuts as autocomplete_light

from django.forms import ModelForm, TextInput, Select, CheckboxInput, Textarea
from django import forms
from django.utils.translation import ugettext_lazy as _
from parcels.models import Parcel
from home.forms import HomeForm



class ParcelForm(autocomplete_light.ModelForm):

    class Meta:
        def merge_dicts(x, y):
            """
            Given two dicts, merge them and return
            """
            z = x.copy()
            z.update(y)
            return z

        model = Parcel

        fields = (
            'origin',
            'destination',
            'delivery_date',
            'flexible',
            'tips',
            'size',
            'weight',
            'notes',
        )


        labels = {
            'origin': 'Originating city',
        }


        help_texts = {
            'origin': _('Where the parcel needs to be collected from'),
            'destination': _('Where the parcel needs to be delivered to'),
            'delivery_date': _('Needs to be delivered by this date'),
            'flexible': _('If your delivery date is flexible'),
            'tips': _('If you are happy to tip'),
            'size': _('Approximate size'),
            'weight': _('Approximate weight'),
            'notes': _('The more details you provide the more chances you '
                      'will be contacted by travellers'),
        }

        # Custom widget attrs
        autocomplete_attrs = {
            'data-autocomplete-minimum-characters': 3,
            'placeholder': _('e.g. London'),
            'class': 'input rounded',
        }


        autocomplete_widget_attrs = {
            'style': 'display:block;',
        }


        datepicker_attrs = {
            'class': 'rounded',
            'placeholder': _('Pick a date'),
        }


        bootstrap_attrs = {'class': 'rounded'}


        widgets = {
            'origin': autocomplete_light.ChoiceWidget('CityAutocomplete',
                attrs=merge_dicts(autocomplete_attrs,
                                  {'autofocus': ''}),
                widget_attrs=autocomplete_widget_attrs),

            'destination': autocomplete_light.ChoiceWidget('CityAutocomplete',
                attrs=merge_dicts(autocomplete_attrs,
                                  {'placeholder': _('e.g. New York')}),
                widget_attrs=autocomplete_widget_attrs),

            'delivery_date': TextInput(attrs=merge_dicts(datepicker_attrs,
                {'required': True})),

            'size': Select(attrs=merge_dicts(bootstrap_attrs,
                {'required': True})),

            'weight': Select(attrs=merge_dicts(bootstrap_attrs,
                {'required': True})),

            'flexible': CheckboxInput(attrs=bootstrap_attrs),

            'tips': CheckboxInput(attrs=bootstrap_attrs),

            'notes': Textarea(attrs=bootstrap_attrs),
        }


class ParcelListForm(HomeForm):

    date = forms.DateField(widget=forms.DateInput(attrs={
            'class': 'form-control',
            'placeholder': _('Delivery date'),
        }
    ), required=False)

    class Meta:
        fields = (
            'fr',
            'to',
            'date',
        )
