/* The code to enable/disable city field when country is defined.
This also adds country_id to GET call to autocomplete */
$(document).ready(function() {
    var citySelectElement = $('#id_city');
    var cityWidgetElement = citySelectElement.parents('.autocomplete-light-widget');
    value = $('.autocomplete-light-widget select[name$=country]').val();
    if (value) {
        $('#id_city-autocomplete').prop('disabled', false);
        cityWidgetElement.yourlabsWidget().autocomplete.data = {
                'country_id': value[0],
        };
    } else {
        $('#id_city-autocomplete').prop('disabled', true);
    }
    $('body').on('change', '.autocomplete-light-widget select[name$=country]', function() {
        var citySelectElement = $('#' + $(this).attr('id').replace('country', 'city'));
        var cityWidgetElement = citySelectElement.parents('.autocomplete-light-widget');

        // When the country select changes
        value = $(this).val();

        if (value) {
            // If value is contains something, add it to autocomplete.data
            cityWidgetElement.yourlabsWidget().autocomplete.data = {
                'country_id': value[0],
            };
            $('#id_city-autocomplete').prop('disabled', false);
        } else {
            // If value is empty, empty autocomplete.data
            cityWidgetElement.yourlabsWidget().autocomplete.data = {}
            cityWidgetElement.yourlabsWidget().freeDeck();
            $('#id_city-autocomplete').prop('disabled', true);
        }
        // example debug statements, that does not replace using breakbpoints and a proper debugger but can hel
        // console.log($(this), 'changed to', value);
        // console.log(cityWidgetElement, 'data is', cityWidgetElement.yourlabsWidget().autocomplete.data)
    })
});