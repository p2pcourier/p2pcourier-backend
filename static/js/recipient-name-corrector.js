/*
There's a bug in django_messages app: when user tries to reply to a message
then in the recipient field instead of "username" they get "[<MyUser: username>]".

The script below fixes it by placing the "username" in the recipient field
*/

$(document).ready(function(){
    var text = $("#id_recipient").val();
    var front = text.match(/^\[</);
    var end = text.match(/>\]$/);
    if (front && end) {
        var username = text.match(/:\s(\w+)/);
        $("#id_recipient").val(username[1]);        
    };
});