/*
This js code clicks the hidden django generated button if user
clicks the button on their profile picture
*/

var fakeButton = document.getElementById("fakeButton");

var realButton = document.getElementById("id_profile_pic");

var saveButton = document.getElementById("saveChangesButton");

fakeButton.addEventListener("click", function(){
    realButton.click();
});

realButton.onchange = function() {
    saveButton.click();
};