/*
This js code provides the following functionality:

button selectAll - selects all messages on the current page
button markUnread - marks the selected messages as read
button delete - deletes the selected messages
button unDelete - moves the messages back to the corresponding 'folders'
*/

$(document).ready(function() {
    
    $("#selectAll").toggle(
        function(){
            $("[type='checkbox']").attr("checked", "checked");
            $(this).text("Unselect all");
        },
        function(){
            $("[type='checkbox']").removeAttr("checked");
            $(this).text("Select all");}
    );


/*  $("#markUnread").click(function(){
        var $boxes = $("input[type='checkbox']:checked");
        $boxes.each(function(){
            var label = $(this).closest('label');
            var spanClass = "icon-lg.icon-custom.icon-line.icon-envelope.color-green";
            if (!(label.find('.'+spanClass).length)){
                label.append(
                '<span aria-hidden="true" class="icon-lg icon-custom \
                icon-line icon-envelope color-green"></span>');
                label.parent().parent().parent().css("font-weight", "bold");
            }
        });
    });*/


    $("#markRead").click(function(){
        var $boxes = $("[id^='msg']");
        $boxes.each(function(){
            var id = parseInt(this.id.replace("msg", ""), 10);
            //var label = $(this).closest('label');
            //var spanClass = "icon-lg.icon-custom.icon-line.icon-envelope.color-green";
            //if (label.find('.'+spanClass).length){
            if($(this).is(':checked')){
                //label.children("span:last").remove();
                //label.parent().parent().parent().css("font-weight", "normal");
                $.get("/messages/view/"+id+"/");
                location.reload(true);
            }
        });
    });
 
 
    $("#delete").click(function(){
        var $boxes = $("[id^='msg']");
        $boxes.each(function(){
            var id = parseInt(this.id.replace("msg", ""), 10);
            if($(this).is(':checked')){
                $.get("/messages/delete/"+id+"/");
                location.reload(true);
                //$.get("/messages/inbox/");
                //setTimeout(function(){window.location.reload(true);},10);
            }
        });
    });


    // this function makes sure that "checked" attr added not only
    // magically "prop" but also visually "attr"
    $("[id^='msg']").click(function(){
        if($(this).is(':checked')){
            $(this).prop('checked', false);
            $(this).attr("checked", "checked");
        } else {
            $(this).prop('checked', true);
            $(this).removeAttr("checked");
        }; 
    });


    $("#unDelete").click(function(){
        var $boxes = $("[id^='msg']");
        $boxes.each(function(){
            var id = parseInt(this.id.replace("msg", ""), 10);
            if($(this).is(':checked')){
                $.get("/messages/undelete/"+id+"/");
                location.reload(true);
                //$.get("/messages/inbox/");
                //setTimeout(function(){window.location.reload(true);},10);
            }
        });
    });



});