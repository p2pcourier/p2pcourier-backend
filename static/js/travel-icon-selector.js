/*
This js code dinamically updates travel type icons on travel
create and travel update html pages
*/

var selectMenu = document.getElementById("id_journey_type");
classData = "icon-custom icon-custom-detail icon-color-u icon-lg rounded-x icon-line ";

window.addEventListener("load", getIcon, false);

selectMenu.addEventListener("change", getIcon);

var dynamicIcon = document.getElementById("dynamicIcon");

function getIcon() {
    var chosenoption = selectMenu.value;
    if (chosenoption == "road"){
        dynamicIcon.className=classData+"icon-transport-028";
        return;
    }
    if (chosenoption == "sea"){
        dynamicIcon.className=classData+"icon-transport-012";
        return;
    }
    if (chosenoption == "air"){
        dynamicIcon.className=classData+"icon-transport-026";
        return;
    }
    if (chosenoption == "train"){
        dynamicIcon.className=classData+"icon-transport-043";
        return;
    }
    if (chosenoption == "walk"){
        dynamicIcon.className=classData+"icon-travel-074";
        return;
    }
    if (chosenoption == "interstellar"){
        dynamicIcon.className=classData+"icon-transport-095";
        return;
    }
    else {
        dynamicIcon.className=classData+"icon-transport-042";
        return;
    }
}
