// Custom js code
$(document).ready(function() {
    
    //This is just an example, you can use this inside any template as:
    //<button id="about-btn-test">Click Me</button>
    $("#about-btn-test").click( function(event) {
        alert("You clicked the button using JQuery!");
    });

});
