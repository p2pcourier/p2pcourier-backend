/*
This js code by default selects Travel model radio button on
page load. If the 'search' button clicked this script sets the 
corresonding action value to the form depending on the model selected

#id_models_1 - is for Travel model radio button
#id_models_0 - is for Parcel model radio button
#mainSearchButton - is for 'search' button
*/

$(document).ready(function() {
    if (window.location.href.indexOf("requests") == -1){
        $("#id_models_0").removeAttr("checked");
        $("#id_models_1").attr("checked", "checked");
    } else {
        $("#id_models_1").removeAttr("checked");
        $("#id_models_0").attr("checked", "checked");
    }
    
    $("#id_models_1").click(function(){
        $("#id_models_1").attr("checked", "checked");
        $("#id_models_0").removeAttr("checked");
    });

    $("#id_models_0").click(function(){
        $("#id_models_0").attr("checked", "checked");
        $("#id_models_1").removeAttr("checked");
    });

    $("#mainSearchButton").click(function(){
        var form = $("#searchbox");
        if($("#id_models_1").is(":checked")){
            form.attr("action", "/search-travels/");
        } else if($("#id_models_0").is(":checked")){
            form.attr("action", "/search-requests/");
        }
    });

});