var map;
$(document).ready(function(){
  var originLatitude = parseFloat(document.getElementById("origin-latitude").value);
  var originLongitude = parseFloat(document.getElementById("origin-longitude").value);
  var destinationLatitude = parseFloat(document.getElementById("destination-latitude").value);
  var destinationLongitude = parseFloat(document.getElementById("destination-longitude").value);
  map = new GMaps({
    div: '#map',
    lat: (originLatitude + destinationLatitude) / 2,
    lng: (originLongitude + destinationLongitude) / 2,
    scrollwheel: false,
  });
  map.addMarker({
    lat: originLatitude,
    lng: originLongitude,
    title: 'From',
    animation: google.maps.Animation.DROP,
  });
  map.addMarker({
    lat: destinationLatitude,
    lng: destinationLongitude,
    title: 'To',
    animation: google.maps.Animation.DROP,
  });
  var latlngs = [{lat: originLatitude, lng: originLongitude}, {lat: destinationLatitude, lng: destinationLongitude}];
  var bounds = [];
  for (var i in latlngs) {
    var latlng = new google.maps.LatLng(latlngs[i].lat, latlngs[i].lng);
    bounds.push(latlng);
    map.addMarker({
      lat: latlngs[i].lat,
      lng: latlngs[i].lng
    });
  }
  map.fitLatLngBounds(bounds);
});