import datetime
import autocomplete_light.shortcuts as autocomplete_light
from django import forms
from django.utils.translation import ugettext_lazy as _
from haystack.forms import model_choices


class HomeForm(forms.Form):
    """
    Form to handle the search
    """
    DATE_CHOICES = (
        (0, _('+/- 7 days')),
        (1, _('+/- 3 days')),
        (2, _('Earlier than this date')),
        (3, _('Later than this date')),
        (4, _('Exact date')),
    )
    date_options = forms.ChoiceField(widget=forms.Select(attrs={
            'class': 'form-control',
        }
    ), choices=DATE_CHOICES, required=False)
    fr = forms.CharField(widget=autocomplete_light.TextWidget('CityAutocomplete',
        attrs={
            'class': 'form-control',
            'placeholder': _('From this city'),
            'data-autocomplete-minimum-characters': 3,
        }
    ), required=False)

    to = forms.CharField(widget=autocomplete_light.TextWidget('CityAutocomplete',
        attrs={
            'class': 'form-control',
            'placeholder': _('To this city'),
            'data-autocomplete-minimum-characters': 3,
        }
    ), required=False)

    # Update this variable to add more models (radio buttons)
    models = forms.ChoiceField(
        required=True,
        choices=model_choices(),
        widget=forms.RadioSelect(attrs={'class': 'radio'}),
        # the initial not working, js script used instead
        # initial={"models": model_choices()[0][1].encode('utf8')},
    )
