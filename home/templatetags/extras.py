from bs4 import BeautifulSoup
from django import template
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def navactive(request, urls, args=None, cls="active"):
    """
    Returns "active" class if current page is the same as request
    """
    for url in urls.split():
        if args:
            try:
                if request.path == reverse(url, args=(args,)):
                    return cls
            except:
                pass
        else:
            if request.path == reverse(url):
                return cls
    return ""


@register.simple_tag
def label_with_classes(value, arg):
    """
    Will change the label class of given value and add arg class(es) to it.
    """
    soup = BeautifulSoup(str(value), "html.parser")
    soup.label['class'] = arg
    itag = soup.new_tag('i')
    soup.label.append(itag)
    return mark_safe(soup)


@register.simple_tag
def debug_object(obj):
    """
    Label tag which does nothing but prints the object details in Terminal
    Usage:
    Ensure that you have this in top of the template: {% load extras %}
    Call it: {% debug_object some_object %}
    """
    print obj
    print dir(obj)


# Note sure if the below filter is used anywhere now - 
# after i added addcss_reg_form?
@register.filter
def addcss(field, css):
    """
    Adds css to fields in templates
    """
    return field.as_widget(attrs={"class":css})


@register.filter
def addcss_reg_form(field, css):
    """
    Adds css to fields in Registration and Login forms
    """
    css_attr = [i.strip() for i in css.split(",")]
    return field.as_widget(attrs={"class":css_attr[0], "placeholder":css_attr[1]})