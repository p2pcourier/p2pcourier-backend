from django.contrib import admin
from djangoseo.admin import register_seo_admin
from home.seo import P2PMetadata

register_seo_admin(admin.site, P2PMetadata)
