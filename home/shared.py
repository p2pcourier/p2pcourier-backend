# This module contains shared functions among apps.
from django.conf import settings
from django.shortcuts import redirect


# helper to be used in urls.py to disallow registration for logged in users
def anonymous_required(func):
    def as_view(request, *args, **kwargs):
        redirect_to = kwargs.get('next', settings.LOGIN_REDIRECT_URL)
        if request.user.is_authenticated():
            return redirect(redirect_to)
        response = func(request, *args, **kwargs)
        return response
    return as_view