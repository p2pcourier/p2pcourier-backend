import random

from django.views.generic import FormView
from django.utils.translation import ugettext as _
from home.forms import HomeForm
from travels.models import Travel
from parcels.models import Parcel


class IndexView(FormView):
    """
    View to render the home page
    """
    template_name = 'home/index.html'
    form_class  = HomeForm

    @property
    def carousel_text(self):
        carousel_text_options = [
           _("Make money while travelling!"),
           _("Monetize your recurring trips by regularly posting and updating "
                "your travel posts!"),
           _("Join our crowdshipping community and get things delivered for "
                "free to all places around the world!"),
        ]
        item_id = random.randrange(len(carousel_text_options))
        return carousel_text_options[item_id]

    def latest_travels(self, amount=4):
        """
        Method which gets latest travels. Ideally should get cached data to
        reduce disk IO.

        :param amount: Optional number of items to get
        :type amount: int
        :returns: List of travels.
        """
        items = Travel.objects.all().order_by('-modified_date')[:amount]
        return items

    def latest_parcels(self, amount=4):
        """
        Method which gets latest requests.

        :param amount: Optional number of items to get
        :type amount: int
        :returns: List of requests.
        """
        items = Parcel.objects.all().order_by('-modified_date')[:amount]
        return items
