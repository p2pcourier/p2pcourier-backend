from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse


class HomeStaticViewSitemap(Sitemap):
    priority = 1.0
    changefreq = 'weekly'

    def items(self):
        return ['index']

    def location(self, item):
        return reverse(item)
