# Builds and runs a local version
local:
	@until nc -z db 3306 ; do \
	  echo "MySQL is unavailable - sleeping" ; \
	  sleep 1 ; \
	done
	@until nc -z rabbit 5672 ; do \
	  echo "Rabbit is unreachable - sleeping" ; \
	  sleep 1 ; \
	done
	@echo "Applying database migrations ..."
	python manage.py migrate
	@echo "Creating default users:"
	@echo "'admin' user with password 'admin' ..."
	@echo "'vusal' user with password 'ostrax123' ..."
	@echo "'tofiq' user with password 'ostrax123' ..."
	python manage.py loaddata misc/docker-django-users.json
	@echo "Loading cities_light fixtures"
	python manage.py cities_light_fixtures load --base-url file:misc/
	@echo "Starting Celery in multi mode ..."
	celery multi start worker -A p2pcourier
	@echo "Starting Django server ..."
	python manage.py runserver 0.0.0.0:8069

# Builds elastic beanstalk compatible package except migrations.
eb:
	# So far we are using local rabbit and celery, this may change in future.
	@until nc -z localhost 5672 ; do \
	  echo "Rabbit is unreachable - sleeping" ; \
	  sleep 1 ; \
	done
	@echo "Collecting statics ..."
	django-admin.py collectstatic --noinput
	@echo "Starting Celery in multi mode ..."
	celery multi start worker -A p2pcourier
	@echo "Creating index folder ..."
	mkdir -p -m 777 /opt/whoosh
	@echo "Building search index ..."
	django-admin.py rebuild_index --noinput
	@echo "Fixing permissions for whoosh folder ..."
	chmod -R 777 /opt/whoosh
	@echo "Loading additional fixtures ..."
	django-admin.py loaddata misc/robots.json
	django-admin.py loaddata misc/sites.json
	django-admin.py loaddata misc/djangoseo.json
	# @echo "Generating cities index (this might take a while) ..."
	#django-admin.py cities_light
