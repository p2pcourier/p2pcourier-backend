import datetime
from haystack import indexes
from travels.models import Travel


class TravelIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Travel index with text field which defines all model fields to be indexed
    in a template and additional filters.
    """
    text = indexes.CharField(document=True, use_template=True)
    date_travel = indexes.DateTimeField(model_attr='journey_start')
    origin_travel = indexes.CharField(model_attr='origin')
    destination_travel = indexes.CharField(model_attr='destination')

    def get_model(self):
        return Travel

    def __str__(self):
        return 'test'