from django.contrib import admin
from travels.models import Travel


class TravelAdmin(admin.ModelAdmin):
    list_display = ('traveller', 'origin', 'destination', 'journey_start',
                    'past_travel', 'slug')
    list_filter = ('over', )
    search_fields = ('traveller__username', 'destination__name')
    raw_id_fields = ('origin', 'destination')

admin.site.register(Travel, TravelAdmin)
