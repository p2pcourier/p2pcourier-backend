from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField
from home.models import BaseModel


@python_2_unicode_compatible
class Travel(BaseModel):
    """
    Model describing the present and past travels
    """
    traveller = models.ForeignKey(settings.AUTH_USER_MODEL,
        verbose_name=_('traveller'), on_delete=models.CASCADE)
    origin = models.ForeignKey('cities_light.City', null=True,
        verbose_name=_('origin'), related_name='origin')
    destination = models.ForeignKey('cities_light.City', null=True,
        verbose_name=_('destination'), related_name='destination')
    journey_start = models.DateField(_("Journey start date"))
    journey_end = models.DateField(_("Journey end date"), blank=True)
    over = models.BooleanField(_("Travel is over?"), default=False)
    recurring = models.BooleanField(_("Recurring travel"), default=False)
    free = models.BooleanField(_("Can carry for free"), default=False)
    slug = AutoSlugField(populate_from='get_slug_name', always_update=True)

    # Size choices
    SIZES = (
        ('small', _('Small (roughly A5-paper size parcel)')),
        ('medium', _('Medium (roughly A4-paper size parcel)')),
        ('large', _('Large (bigger parcel)')),
        (None, _('Please pick a size')),
    )

    # Weight choices, should be localised depending on locale
    WEIGHTS = (
        ('light', _('Light (up to 0.5 kg)')),
        ('medium', _('Medium (up to 1 kg)')),
        ('heavy', _('Heavy (more than 1 kg)')),
        (None, _('Please pick a weight')),
    )

    # Travel type
    TYPES = (
        ('road', _('Road')),
        ('sea', _('Sea')),
        ('air', _('Air')),
        ('train', _('Rail')),
        ('walk', _('Walk')),
        ('interstellar', _('Interstellar')),
        (None, _('Optionally pick a journey type')),
    )

    max_size = models.CharField(max_length=10, choices=SIZES)
    max_weight = models.CharField(max_length=10, choices=WEIGHTS)
    journey_type = models.CharField(max_length=15, choices=TYPES, blank=True,
                                    default=None)

    def __str__(self):
        if self.traveller.first_name:
            name = self.traveller.first_name
        else:
            name = self.traveller.username
        return _(u"{} travels from {}, {} to {}, {} on {}").format(
            name, self.origin.name_ascii, self.origin.country.name_ascii,
            self.destination.name_ascii, self.destination.country.name_ascii,
            self.journey_start.strftime('%d %B %Y'))

    def past_travel(self):
        now = timezone.now().date()
        return self.journey_start < now

    past_travel.admin_order_field = 'journey_start'
    past_travel.boolean = True
    past_travel.short_description = _('Past travel?')

    def clean(self):
        if not self.journey_end:
            self.journey_end = self.journey_start
        # journey_end should not be before journey_start
        if self.journey_end < self.journey_start:
            raise ValidationError({
                'journey_end': _('Journey end time can not be before '
                                  'start time!')
            })

    def get_absolute_url(self):
        """
        Handy way of getting the url of the object to its detail view page
        """
        return reverse('travel-detail', args=(str(self.id), self.slug))

    def get_slug_name(self):
        """
        Return a slug name for this model
        """
        return "{}-{}".format(self.origin.name_ascii.lower(),
            self.destination.name_ascii.lower())

    class Meta:
        verbose_name = _('travel')
        verbose_name_plural = _('travels')
