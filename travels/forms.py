import autocomplete_light.shortcuts as autocomplete_light

from django.forms import ModelForm, TextInput, Select, CheckboxInput
from django import forms
from django.utils.translation import ugettext_lazy as _
from travels.models import Travel
from home.forms import HomeForm


class TravelForm(autocomplete_light.ModelForm):

    class Meta:
        def merge_dicts(x, y):
            """
            Given two dicts, merge them and return
            """
            z = x.copy()
            z.update(y)
            return z

        model = Travel

        fields = (
            'origin',
            'destination',
            'journey_start',
            'journey_end',
            'journey_type',
            'max_size',
            'max_weight',
            'free',
            'over',
        )
        labels = {
            'origin': _('From'),
            'destination': _('To'),
        }
        help_texts = {
            'origin': _('Start typing the name of the city you are travelling'
                       ' from'),
            'destination': _('Start typing the name of the city you are travel'
                            'ling to'),
            'journey_start': _('Date when you intend to travel'),
            'journey_end': _('Optionally include end date'),
        }
        # Custom widget attrs
        autocomplete_attrs = {
            'data-autocomplete-minimum-characters': 3,
            'placeholder': _('e.g. London'),
            'class': 'input rounded',
        }
        autocomplete_widget_attrs = {
            'style': 'display:block;',
        }
        datepicker_attrs = {
            'placeholder': _('Pick a date'),
            'class': 'rounded',
        }
        bootstrap_attrs = {'class': 'rounded'}
        widgets = {
            'origin': autocomplete_light.ChoiceWidget('CityAutocomplete',
                attrs=merge_dicts(autocomplete_attrs,
                                  {'autofocus': ''}),
                widget_attrs=autocomplete_widget_attrs),
            'destination': autocomplete_light.ChoiceWidget('CityAutocomplete',
                attrs=merge_dicts(autocomplete_attrs,
                                  {'placeholder': _('e.g. New York')}),
                widget_attrs=autocomplete_widget_attrs),
            'journey_start': TextInput(attrs=merge_dicts(datepicker_attrs,
                {'required': True})),
            'journey_end': TextInput(attrs=datepicker_attrs),
            'max_size': Select(attrs=merge_dicts(bootstrap_attrs,
                {'required': True})),
            'max_weight': Select(attrs=merge_dicts(bootstrap_attrs,
                {'required': True})),
            'journey_type': Select(attrs=bootstrap_attrs),
            'free': CheckboxInput(attrs=bootstrap_attrs),
            'over': CheckboxInput(attrs=bootstrap_attrs),
        }


class TravelListForm(HomeForm):

    date = forms.DateField(widget=forms.DateInput(attrs={
            'class': 'form-control',
            'placeholder': _('Jorney start date'),
        }
    ), required=False)

    class Meta:
        fields = (
            'fr',
            'to',
            'date',
        )
