import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.core.exceptions import ValidationError
from django.utils import timezone

from cities_light.models import Country, City
from travels.models import Travel
from travels.forms import TravelForm


@override_settings(CELERY_ALWAYS_EAGER=True)
class TravelTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        """
        Test setup function with test database fixtures. This is executed once
        for all tests in this class.
        """
        cls.country = Country.objects.create(name='Westeros')
        cls.city = City.objects.create(name='Winterfell', country=cls.country)
        cls.city2 = City.objects.create(name='Meereen', country=cls.country)
        cls.user = get_user_model().objects.create_user('john.snow',
            'john@wc.we', 'TheW@ll')
        cls.user2 = get_user_model().objects.create_user('Daenerys_Targaryen',
            'khalese@wc.we', 'Dr@G0n')
        cls.destination_city = City.objects.create(name='Kings Landing',
                                                   country=cls.country)
        cls.destination_city2 = City.objects.create(name='Volantis',
                                                   country=cls.country)
        cls.today = timezone.now().date()
        cls.travel = Travel.objects.create(
            traveller=cls.user,
            origin=cls.city,
            destination=cls.destination_city,
            journey_start=cls.today,
            journey_end=cls.today + datetime.timedelta(days=2),
            journey_type='walk',
            max_size='medium',
            max_weight='light',
            over=False,
            recurring=False,
            free=True,
        )
        cls.travel2 = Travel.objects.create(
            traveller=cls.user2,
            origin=cls.city2,
            destination=cls.destination_city2,
            journey_start=cls.today + datetime.timedelta(days=10),
            journey_end=cls.today + datetime.timedelta(days=15),
            journey_type='walk',
            max_size='medium',
            max_weight='light',
            over=False,
            recurring=False,
            free=True,
        )

    def test_travel_model(self):
        """
        Tests travel methods.
        """
        self.assertFalse(self.travel.past_travel())

    def test_travel_validation(self):
        """
        This should raise an exception when travel end time is earlier than
        its start time.
        """
        travel = Travel.objects.create(
                traveller=self.user,
                origin=self.city,
                destination=self.destination_city,
                journey_start=self.today + datetime.timedelta(days=2),
                journey_end=self.today,
                journey_type='train',
                max_size='large',
                max_weight='heavy',
                over=False,
                recurring=True,
                free=False,
            )
        self.assertRaisesMessage(
            ValidationError,
            'Journey end time can not be before start time!',
            travel.clean
        )

    def test_travels_list(self):
        """
        Tests to get the list of travels.
        """
        response = self.client.get('/travels/')
        self.assertTrue(len(response.context['travel_list']) == 2)
        self.assertContains(response, 'Kings Landing')
        self.assertContains(response, 'Volantis')
        self.assertTemplateUsed(response, 'travels/travel_list.html')

    def test_travels_list_search(self):
        """
        Tests travel list search.
        """
        response = self.client.get('/travels/', {
            'from': 'Winterfell',
            'to': 'Kings Landing',
            'date': self.today.strftime('%Y-%m-%d')})
        self.assertContains(response, 'Winterfell')
        self.assertTemplateUsed(response, 'travels/travel_list.html')
        self.assertTrue(len(response.context['travel_list']) == 2)

    def test_travel_detail(self):
        """
        Tests travel detail.
        """
        login = self.client.login(username='john.snow', password='TheW@ll')
        self.assertTrue(login)
        response = self.client.get('/travels/1/winterfell-kings-landing/')
        self.assertContains(response, 'Kings Landing')
        self.assertNotContains(response, 'Meereen')
        self.assertTemplateUsed(response, 'travels/travel_detail.html')
        self.client.logout()

    def test_travel_create(self):
        """
        Tests travel creation.
        """
        login = self.client.login(username='john.snow', password='TheW@ll')
        self.assertTrue(login)
        response = self.client.post('/travels/new',
            {
                'origin': self.city.id,
                'destination': self.city2.id,
                'journey_start': self.today,
                'journey_end': self.today,
                'journey_type': 'road',
                'max_size': 'medium',
                'max_weight': 'medium',
                'free': False,
                'over': False,
            },
            follow=True)
        self.assertContains(response, 'Winterfell')
        self.assertContains(response, 'Travel was successfully created')
        self.assertTemplateUsed(response, 'travels/travel_detail.html')
        self.client.logout()

    def test_travel_create_bad_form(self):
        """
        Tests travel creation with bad form. Shall raise an exception.
        """
        login = self.client.login(username='john.snow', password='TheW@ll')
        self.assertTrue(login)
        response = self.client.post('/travels/new',
            {
                'origin': 10,
                'journey_start': 'tomorrow',
                'journey_end': 'bazar_gunu',
                'journey_type': 'xalturshik',
                'max_size': 'qoz boyda',
                'max_weight': 100,
            },
            follow=True)
        self.assertFormError(response, 'form', 'origin',
            ('Select a valid choice. That choice is not one of the available '
             'choices.'))
        self.assertFormError(response, 'form', 'destination',
            'This field is required.')
        self.assertFormError(response, 'form', 'journey_start',
                             'Enter a valid date.')
        self.assertFormError(response, 'form', 'journey_end',
                             'Enter a valid date.')
        self.assertFormError(response, 'form', 'journey_type',
            ('Select a valid choice. xalturshik is not one of the available '
             'choices.'))
        self.assertFormError(response, 'form', 'max_size',
            ('Select a valid choice. qoz boyda is not one of the available '
             'choices.'))
        self.assertFormError(response, 'form', 'max_weight',
            'Select a valid choice. 100 is not one of the available choices.')
        # We don't want non-field errors
        self.assertFormError(response, 'form', None, None)

    def test_travel_update(self):
        """
        Tests travel update.
        """
        login = self.client.login(username='Daenerys_Targaryen',
                                  password='Dr@G0n')
        self.assertTrue(login)
        # First we create a travel
        response = self.client.post('/travels/new',
            {
                'origin': self.city.id,
                'destination': self.city2.id,
                'journey_start': self.today,
                'journey_end': self.today,
                'journey_type': 'sea',
                'max_size': 'small',
                'max_weight': 'heavy',
                'free': True,
                'over': False,
            },
            follow=True)
        self.assertContains(response, 'Sea')
        # Update the same
        response = self.client.post(
            '/travels/edit/{}/{}/'.format(self.travel.id, self.travel.slug),
            {
                'origin': self.city.id,
                'destination': self.city2.id,
                'journey_start': self.today,
                'journey_end': self.today + datetime.timedelta(days=5),
                'journey_type': 'air',
                'max_size': 'medium',
                'max_weight': 'medium',
                'free': False,
                'over': False,
            },
            follow=True)
        self.assertContains(response, 'Air')
        self.assertNotContains(response, 'Sea')
        self.assertTemplateUsed(response, 'travels/travel_detail.html')
        self.client.logout()

    def test_travel_delete(self):
        """
        Tests travel deletion.
        """
        login = self.client.login(username='john.snow', password='TheW@ll')
        self.assertTrue(login)
        response = self.client.post('/travels/new',
            {
                'origin': self.city.id,
                'destination': self.city2.id,
                'journey_start': self.today,
                'journey_end': self.today,
                'journey_type': 'road',
                'max_size': 'medium',
                'max_weight': 'medium',
                'free': False,
                'over': False,
            },
            follow=True)
        response = self.client.post(
            '/travels/delete/{}/{}/'.format(self.travel.id, self.travel.slug),
            follow=True)
        self.assertContains(response, 'Travel successfully deleted')
        self.client.logout()

    def test_anonymous_create_fail(self):
        """
        Test to check if anonymous user redirected to login page
        when they try to create a travel.
        """
        response = self.client.post('/travels/new',
            {
                'origin': self.city.id,
                'destination': self.city2.id,
                'journey_start': self.today,
                'journey_end': self.today,
                'journey_type': 'road',
                'max_size': 'medium',
                'max_weight': 'medium',
                'free': False,
                'over': False,
            },
            follow=True)
        self.assertRedirects(response, '/accounts/login/?next=/travels/new')

    def test_anonymous_update_fail(self):
        """
        Test to check if anonymous user redirected to login page
        when they try to create a travel.
        """
        travel = Travel.objects.all()[0]
        response = self.client.post('/travels/edit/{}/{}/'.format(travel.id,
                travel.slug),
            {
                'origin': self.city.id,
                'destination': self.city2.id,
                'journey_start': self.today,
                'journey_end': self.today,
                'journey_type': 'road',
                'max_size': 'medium',
                'max_weight': 'medium',
                'free': False,
                'over': False,
            },
            follow=True)
        self.assertRedirects(response,
            '/accounts/login/?next=/travels/edit/{}/{}/'.format(travel.id,
                travel.slug))
