from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from travels import views

urlpatterns = [
    url(r'^$', views.TravelList.as_view(), name='travels'),
    url(r'^(?P<from>\w+)(?P<to>\w+)(?P<date>\d{4}-\d{2}-\d{2})$',
        views.TravelList.as_view(), name='travels-search'),
    url(r'^(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$',
        login_required(views.TravelDetail.as_view()), name='travel-detail'),
    url(r'^new$', login_required(views.TravelCreate.as_view()),
        name='travel-new'),
    url(r'^edit/(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$',
        login_required(views.TravelUpdate.as_view()), name='travel-edit'),
    url(r'^delete/(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$',
        login_required(views.TravelDelete.as_view()), name='travel-delete'),
]