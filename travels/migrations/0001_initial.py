# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('cities_light', '0004_auto_20160108_1552'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Travel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('journey_start', models.DateField(verbose_name=b'Journey start date and time')),
                ('journey_end', models.DateField(verbose_name=b'Journey end date and time', blank=True)),
                ('over', models.BooleanField(default=False, verbose_name=b'Travel is over?')),
                ('recurring', models.BooleanField(default=False, verbose_name=b'Recurring travel')),
                ('free', models.BooleanField(default=False, verbose_name=b'Can carry for free')),
                ('max_size', models.CharField(max_length=10, choices=[(b'small', b'Small (A5 document, small bag or parcel)'), (b'medium', b'Medium (A4 document, medium bag or parcel)'), (b'large', b'Any size'), (None, b'Please pick a size')])),
                ('max_weight', models.CharField(max_length=10, choices=[(b'light', b'Light (no more than 500 gr.)'), (b'medium', b'Medium (no more than 1 kg.)'), (b'heavy', b'Any weight'), (None, b'Please pick a weight')])),
                ('journey_type', models.CharField(default=None, max_length=15, blank=True, choices=[(b'road', b'Road'), (b'sea', b'Sea'), (b'air', b'Air'), (b'train', b'Rail'), (b'walk', b'Walk'), (b'interstellar', b'Interstellar'), (None, b'Optionally pick a journey type')])),
                ('destination', models.ForeignKey(related_name='destination', to='cities_light.City', null=True)),
                ('origin', models.ForeignKey(related_name='origin', to='cities_light.City', null=True)),
                ('traveller', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
