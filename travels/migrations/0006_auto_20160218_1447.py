# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('travels', '0005_auto_20160124_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='travel',
            name='journey_end',
            field=models.DateField(verbose_name=b'Journey end date', blank=True),
        ),
        migrations.AlterField(
            model_name='travel',
            name='journey_start',
            field=models.DateField(verbose_name=b'Journey start date'),
        ),
        migrations.AlterField(
            model_name='travel',
            name='max_size',
            field=models.CharField(max_length=10, choices=[(b'small', b'Small (roughly A5-paper size parcel)'), (b'medium', b'Medium (roughly A4-paper size parcel)'), (b'large', b'Large (bigger parcel)'), (None, b'Please pick a size')]),
        ),
        migrations.AlterField(
            model_name='travel',
            name='max_weight',
            field=models.CharField(max_length=10, choices=[(b'light', b'Light (up to 0.5 kg)'), (b'medium', b'Medium (up to 1 kg)'), (b'heavy', b'Heavy (more than 1 kg)'), (None, b'Please pick a weight')]),
        ),
    ]
