# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('travels', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='travel',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 24, 15, 56, 15, 646481, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='travel',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 24, 15, 56, 21, 682346, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
