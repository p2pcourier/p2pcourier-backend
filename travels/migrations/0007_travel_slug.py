# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('travels', '0006_auto_20160218_1447'),
    ]

    operations = [
        migrations.AddField(
            model_name='travel',
            name='slug',
            field=autoslug.fields.AutoSlugField(default='slug', editable=False, populate_from=b'get_slug_name', always_update=True),
            preserve_default=False,
        ),
    ]
