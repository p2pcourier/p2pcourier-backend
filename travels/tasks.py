from django.conf import settings
from django.contrib.sites.models import Site
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from django.template import loader
from django.utils.translation import ugettext as _
from p2pcourier.celery import app
# from p2pcourier.settings import DEFAULT_FROM_EMAIL as from_email
from parcels.models import Parcel
from travels.models import Travel


'''
@app.task
def inform_requesters_periodic_task():
    """
    Checks the db and sends emails to requesters
    periodically
    """
    return "Dostoyniy prezident"
'''

@app.task
def inform_requesters_task(travel_id):
    """
    Checks the db and sends emails to requesters
    when a travel created/updated
    """
    # TODO I lost 3 to 4 hours on this..
    from_email = 'noreply@post4tips.com'
    travel = Travel.objects.get(pk=travel_id)

    parcels = Parcel.objects.select_related().exclude(
        requester_id=str(travel.traveller_id)).exclude(
        requester__match_emails=False)

    parcels = parcels.filter(origin=travel.origin).filter(
        destination=travel.destination)

    parcels = parcels.filter(delivery_date__gte=travel.journey_end)

    if parcels:
        raw_emails = set(parcels.values_list('requester__email'))
        recipient_list = [email[0] for email in raw_emails]
    else:
        return

    subject = _("New travel matching your request")

    domain = Site.objects.get_current()

    html_file = "notifications/new_travel.html"
    text_file = "notifications/new_travel.txt"

    prefix = "http://"
    if 'prod' in settings.SETTINGS_MODULE:
        prefix = "https://"

    data = {
        "origin" : travel.origin,
        "destination" : travel.destination,
        "date" : travel.journey_start,
        "user" : travel.traveller,
        "id" : travel.id,
        "slug" : travel.slug,
        "site" : domain,
        "prefix" : prefix,
    }

    html_content = loader.get_template(html_file).render(data)
    text_content = loader.get_template(text_file).render(data)

    messages = []
    for recipient in recipient_list:
        message = EmailMultiAlternatives(subject, text_content, from_email,
            [recipient])
        message.attach_alternative(html_content, "text/html")
        messages.append(message)

    connection = mail.get_connection()
    connection.send_messages(messages)
