from django.contrib.sitemaps import Sitemap
from travels.models import Travel


class TravelSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.7

    def items(self):
        return Travel.objects.all()

    def lastmod(self, obj):
        return obj.modified_date
