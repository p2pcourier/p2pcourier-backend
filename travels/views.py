from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import Http404
from django.utils.translation import ugettext as _
from django.views import generic
from django.views.generic.edit import FormView
from django_messages.forms import ComposeForm
# from django.http.response import HttpResponseForbidden
from travels.models import Travel
from travels.forms import TravelForm, TravelListForm
from travels.tasks import inform_requesters_task


class TravelList(generic.ListView, FormView):
    """
    List view of all available travels
    """
    template_name = 'travel_list.html'
    queryset = Travel.objects.all().order_by('-journey_start')
    form_class = TravelListForm


class TravelDetail(generic.DetailView):
    """
    Detail view for a single post and an apply form
    TODO: Currently recipient and subject are just hidden, we need to modify
    forms to ignore changes coming from post and use details we want, i.e.
    someone can "unhide" recipient and subject and send whatever they want
    """
    queryset = Travel.objects.all()

    @property
    def travel_icon(self):
        """
        Return a travel icon depending on type of travel.
        Icons list:
        http://127.0.0.1:8000/static/unify/shortcode_icon_line_transport.html
        """
        icons = {
            'road': 'icon-transport-028',
            'sea': 'icon-transport-012',
            'air': 'icon-transport-026',
            'train': 'icon-transport-043',
            'walk': 'icon-travel-074',
            'interstellar': 'icon-transport-095',
        }
        travel_type = self.get_object().journey_type
        if travel_type in icons:
            return icons[travel_type]
        else:
            return 'icon-transport-042'

    def get_context_data(self, **kwargs):
        context = super(TravelDetail, self).get_context_data(**kwargs)
        travel = self.get_object()
        context['form'] = ComposeForm()
        context['subject'] = \
            _('Delivery request for {} - {} travel').format(
                travel.origin.name_ascii, travel.destination.name_ascii)
        context['message_placeholder'] = _('Please describe your package here.')
        return context


class TravelCreate(generic.CreateView):
    """
    Travel create view
    """
    model = Travel
    form_class = TravelForm

    def form_valid(self, form):
        form.instance.traveller = self.request.user
        return super(TravelCreate, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, _('Travel was successfully created'))
        inform_requesters_task.delay(self.object.id)
        return reverse('travel-detail',
                       args=(self.object.id, self.object.slug))


class TravelUpdate(generic.UpdateView):
    """
    Travel update view
    """
    model = Travel
    form_class = TravelForm

    def get_success_url(self):
        messages.success(self.request, _('Travel successfully updated'))
        inform_requesters_task.delay(self.object.id)
        return reverse('travel-detail',
                        args=(self.object.id, self.object.slug))

class TravelDelete(generic.DeleteView):
    """
    Travel delete view
    """
    model = Travel

    def get_success_url(self):
        messages.success(self.request, _('Travel successfully deleted'))
        return reverse_lazy('travels')

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user """
        travel = super(TravelDelete, self).get_object()
        if not travel.traveller == self.request.user:
            raise Http404
        return travel
