import autocomplete_light.shortcuts as autocomplete_light

from cities_light.contrib.autocompletes import *

class CityAutocomplete(CityAutocomplete):
    search_fields = ('search_names', )
    limit_choices = 10
    attrs = {
        'data-autocomplete-auto-hilight-first': 'true',
    }
    
    def choices_for_request(self):
        self.choices = self.choices.order_by('-population')
        return super(CityAutocomplete, self).choices_for_request()


autocomplete_light.register(City, CityAutocomplete)