from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings



# default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'p2pcourier.settings')

# first argument is the name of the current module, you could also add 
# more arguments here like broker or backend, but on this project all these
# arguments are specified in settings.py
app = Celery('p2pcourier')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# app.conf.update(
#     CELERY_TASK_SERIALIZER = 'json',
#     CELERY_RESULT_SERIALIZER = 'json',
#     CELERY_ACCEPT_CONTENT=['json'],
#     CELERY_TIMEZONE = 'Europe/London',
#     CELERY_ENABLE_UTC = True)

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))