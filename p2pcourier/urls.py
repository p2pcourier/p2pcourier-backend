from django.conf.urls import include, url, patterns, static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.conf import settings
from envelope.views import ContactView
from home.shared import anonymous_required
from home.sitemaps import HomeStaticViewSitemap
from overrides.forms import MyRegistrationForm
from pages.sitemaps import PagesStaticViewSitemap
from registration.backends.default.views import RegistrationView
from search.views import TravelsSearchView, ParcelsSearchView
from travels.sitemaps import TravelSitemap
from parcels.sitemaps import ParcelSitemap

sitemaps = {
    'home': HomeStaticViewSitemap,
    'static': PagesStaticViewSitemap,
    'requests': ParcelSitemap,
    'travels': TravelSitemap,
}

urlpatterns = [
    url(r'^', include('home.urls')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/register/$', anonymous_required(
        RegistrationView.as_view(form_class=MyRegistrationForm)),
        name='registration_register'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^contact/', ContactView.as_view(success_url="/pages/thankyou"),
        name='envelope-contact'),
    url(r'^messages/', include('django_messages.urls', namespace='messages')),
    url(r'^pages/', include('pages.urls')),
    url(r'^travels/', include('travels.urls')),
    url(r'^requests/', include('parcels.urls')),
    url(r'^robots\.txt', include('robots.urls')),
    url(r'^search-travels/', TravelsSearchView(), name='search-travels'),
    url(r'^search-requests/', ParcelsSearchView(), name='search-requests'),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap')
]
