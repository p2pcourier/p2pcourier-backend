from registration.forms import (RegistrationFormTermsOfService,
    RegistrationFormUniqueEmail)

# Registration form override to include TOS field and ensure email uniqueness
class MyRegistrationForm(RegistrationFormTermsOfService,
        RegistrationFormUniqueEmail):
    pass