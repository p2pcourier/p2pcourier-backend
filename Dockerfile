FROM python:2.7
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get install -y \
    netcat \
    libmysqlclient-dev \
    libffi-dev \
    libssl-dev \
    python-dev

COPY requirements.txt /src/
RUN pip install -r /src/requirements.txt
COPY . /src/
EXPOSE 8069
