import autocomplete_light.shortcuts as autocomplete_light
from django import forms
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from haystack.forms import SearchForm, model_choices
from haystack.query import SearchQuerySet
from home.forms import HomeForm
from travels.models import Travel
from parcels.models import Parcel



class TravelsSearchForm(SearchForm, HomeForm):

    date = forms.DateField(widget=forms.DateInput(attrs={
            'class': 'form-control',
            'placeholder': _('Jorney start date'),
        }
    ), required=False)

    def search(self):
        if not self.is_valid():
            return self.no_query_found()
        if not self.cleaned_data.get('fr') and not self.cleaned_data.get('to') \
            and not self.cleaned_data.get('date'):
            return self.no_query_found()
        sqs = SearchQuerySet().models(Travel).order_by('-date_travel')
        if self.cleaned_data.get('fr'):
            sqs = sqs.filter(origin_travel=self.cleaned_data['fr'])
        if self.cleaned_data.get('to'):
            sqs = sqs.filter(destination_travel=self.cleaned_data['to'])
        if self.cleaned_data.get('date'):
            choice = int(self.cleaned_data.get('date_options'))
            if choice == 0:
                dt_range = timezone.timedelta(days=7)
                sqs = sqs.filter(date_travel__range=(self.cleaned_data['date'] \
                    - dt_range, self.cleaned_data['date'] + dt_range))
            elif choice == 1:
                dt_range = timezone.timedelta(days=3)
                sqs = sqs.filter(date_travel__range=(self.cleaned_data['date'] \
                    - dt_range, self.cleaned_data['date'] + dt_range))
            elif choice == 2:
                sqs = sqs.filter(date_travel__lte=self.cleaned_data['date'])
            elif choice == 3:
                sqs = sqs.filter(date_travel__gte=self.cleaned_data['date'])
            elif choice == 4:
                sqs = sqs.filter(date_travel__eq=self.cleaned_data['date'])
        return sqs


class ParcelsSearchForm(SearchForm, HomeForm):

    date = forms.DateField(widget=forms.DateInput(attrs={
            'class': 'form-control',
            'placeholder': _('Delivery date'),
        }
    ), required=False)

    def search(self):
        if not self.is_valid():
            return self.no_query_found()
        if not self.cleaned_data.get('fr') and not self.cleaned_data.get('to') \
            and not self.cleaned_data.get('date'):
            return self.no_query_found()
        sqs = SearchQuerySet().models(Parcel).order_by('-date_parcel')
        if self.cleaned_data.get('fr'):
            sqs = sqs.filter(origin_parcel=self.cleaned_data['fr'])
        if self.cleaned_data.get('to'):
            sqs = sqs.filter(destination_parcel=self.cleaned_data['to'])
        if self.cleaned_data.get('date'):
            choice = int(self.cleaned_data.get('date_options'))
            if choice == 0:
                dt_range = timezone.timedelta(days=7)
                sqs = sqs.filter(date_parcel__range=(self.cleaned_data['date'] \
                    - dt_range, self.cleaned_data['date'] + dt_range))
            elif choice == 1:
                dt_range = timezone.timedelta(days=3)
                sqs = sqs.filter(date_parcel__range=(self.cleaned_data['date'] \
                    - dt_range, self.cleaned_data['date'] + dt_range))
            elif choice == 2:
                sqs = sqs.filter(date_parcel__lte=self.cleaned_data['date'])
            elif choice == 3:
                sqs = sqs.filter(date_parcel__gte=self.cleaned_data['date'])
            elif choice == 4:
                sqs = sqs.filter(date_parcel__eq=self.cleaned_data['date'])
        return sqs
