from haystack.views import SearchView
from haystack.query import SearchQuerySet
from search.forms import TravelsSearchForm, ParcelsSearchForm
from django.conf import settings



class TravelsSearchView(SearchView):
    """
    This SearchView overrides haystack default view and adds three
    additional query arguments: "fr", "to", "date"
    """
    def __init__(self, *args, **kwargs):
        
        self.load_all = True
        self.form_class = TravelsSearchForm
        self.searchqueryset = SearchQuerySet()
        self.template = 'search/travels_search.html'

    def __call__(self, request):
        """
        Generates the actual response to the search.
        Relies on internal, overridable methods to construct the response.
        """
        self.request = request
        self.form = self.build_form()
        self.query = self.get_query()
        self.results = self.get_results()
        self.fr = self.get_fr()
        self.to = self.get_to()
        self.date = self.get_date()

        return self.create_response()

    def get_fr(self):
        """
        Returns the 'fr' provided by the user.
        Returns an empty string if the 'fr' is invalid.
        """
        if self.form.is_valid():
            return self.form.cleaned_data['fr']

        return ''

    def get_to(self):
        """
        Returns the 'to' provided by the user.
        Returns an empty string if the 'to' is invalid.
        """
        if self.form.is_valid():
            return self.form.cleaned_data['to']

        return ''

    def get_date(self):
        """
        Returns the 'date' provided by the user.
        Returns an empty string if the 'date' is invalid.
        """
        if self.form.is_valid():
            if self.form.cleaned_data['date'] == None:
                return ''
            else:
                return self.form.cleaned_data['date']

        return ''


    def get_context(self):
        
        context = {
            'fr': self.fr,
            'to': self.to,
            'date': self.date,
            'form': self.form,
            'results':self.results,
            'suggestion': None,
        }

        check = hasattr(self.results, 'fr') or hasattr(self.results, 'to') \
            or hasattr(self.results, 'date')

        if self.results and check and self.results.query.backend.include_spelling:
            context['suggestion'] = self.form.get_suggestion()

        context.update(self.extra_context())

        return context



class ParcelsSearchView(SearchView):
    """
    This SearchView overrides haystack default view and adds three
    additional query arguments: "fr", "to", "date"
    """
    def __init__(self, *args, **kwargs):
        
        self.load_all = True
        self.form_class = ParcelsSearchForm
        self.searchqueryset = SearchQuerySet()
        self.template = 'search/parcels_search.html'
    
    def __call__(self, request):
        """
        Generates the actual response to the search.
        Relies on internal, overridable methods to construct the response.
        """
        self.request = request
        self.form = self.build_form()
        self.query = self.get_query()
        self.results = self.get_results()
        self.fr = self.get_fr()
        self.to = self.get_to()
        self.date = self.get_date()

        return self.create_response()

    def get_fr(self):
        """
        Returns the 'fr' provided by the user.
        Returns an empty string if the 'fr' is invalid.
        """
        if self.form.is_valid():
            return self.form.cleaned_data['fr']

        return ''

    def get_to(self):
        """
        Returns the 'to' provided by the user.
        Returns an empty string if the 'to' is invalid.
        """
        if self.form.is_valid():
            return self.form.cleaned_data['to']

        return ''

    def get_date(self):
        """
        Returns the 'date' provided by the user.
        Returns an empty string if the 'date' is invalid.
        """
        if self.form.is_valid():
            if self.form.cleaned_data['date'] == None:
                return ''
            else:
                return self.form.cleaned_data['date']

        return ''


    def get_context(self):

        context = {
            'fr': self.fr,
            'to': self.to,
            'date': self.date,
            'form': self.form,
            'results':self.results,            
            'suggestion': None,
        }

        check = hasattr(self.results, 'fr') or hasattr(self.results, 'to') \
            or hasattr(self.results, 'date')

        if self.results and check and self.results.query.backend.include_spelling:
            context['suggestion'] = self.form.get_suggestion()

        context.update(self.extra_context())

        return context   