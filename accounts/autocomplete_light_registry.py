import autocomplete_light.shortcuts as autocomplete_light

from cities_light.contrib.autocompletes import *

class UserAutocomplete(CityAutocomplete):
    search_fields = ('search_names', )
    limit_choices = 10
    order_by = '-population'
    attrs = {
        'data-autocomplete-auto-hilight-first': 'true',
    }
    
    def choices_for_request(self):
        q = self.request.GET.get('q', '')
        country_id = self.request.GET.get('country_id', None)

        choices = self.choices.all()
        if q:
            choices = choices.filter(name_ascii__icontains=q)
        if country_id:
            choices = choices.filter(country_id=country_id)
        else:
            choices = choices.none()

        return self.order_choices(choices)[0:self.limit_choices]


autocomplete_light.register(City, UserAutocomplete)

autocomplete_light.register(Country,
    attrs = {'data-autocomplete-auto-hilight-first': 'true'},
    search_fields=('name', 'name_ascii',)
)