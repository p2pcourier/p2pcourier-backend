import autocomplete_light.shortcuts as autocomplete_light

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from accounts.forms import MyUserForm


class MyUserAdmin(UserAdmin):
    form = MyUserForm
    UserAdmin.fieldsets += (
        ("Additional info", {
            'fields': (
                'phone', 'dob', 'country', 'city','profile_pic',
                'receive_emails', 'match_emails',)
            }
        ),
    )

    UserAdmin.list_display += ('country', 'city')


admin.site.register(get_user_model(), MyUserAdmin)