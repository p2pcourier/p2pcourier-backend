from django.test import TestCase
from accounts.models import MyUser



class MyUserTests(TestCase):
    """
    Test for MyUser model
    """

    def test_str(self):

        a_user = MyUser(
            username = "sheriff",
            first_name = "rick",
            last_name = "grimes",
            email = "walker@cynthiana.com",
        )

        self.assertEquals(str(a_user),'rick grimes',)