# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='myuser',
            name='firstname',
        ),
        migrations.RemoveField(
            model_name='myuser',
            name='lastname',
        ),
        migrations.RemoveField(
            model_name='myuser',
            name='nickname',
        ),
        migrations.RemoveField(
            model_name='myuser',
            name='useremail',
        ),
    ]
