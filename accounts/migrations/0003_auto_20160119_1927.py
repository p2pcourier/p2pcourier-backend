# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20151215_1932'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='profile_pic',
            field=models.ImageField(default=b'avatars/none/def.jpg', upload_to=b'avatars'),
        ),
        migrations.AlterField(
            model_name='myuser',
            name='phone',
            field=models.CharField(max_length=20, null=True, verbose_name=b'Phone number', blank=True),
        ),
    ]
