from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views import generic
from django.views.decorators.debug import sensitive_post_parameters
from django.utils.decorators import method_decorator
from accounts.forms import MyUserForm, MySettingsForm
from travels.models import Travel
from parcels.models import Parcel


class AccountGuestView(generic.DetailView):
    """
    This view shows a user's profile to a guest
    """
    model = get_user_model()
    template_name = "accounts/account_guest_view.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if request.user.username == self.object.username:
            return HttpResponseRedirect(reverse('account-update',
                args=(self.object.id,)))

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class AccountUpdateView(generic.UpdateView):
    """
    This view updates user's data
    """
    model = get_user_model()
    form_class = MyUserForm
    template_name = "accounts/account_update.html"

    @method_decorator(sensitive_post_parameters("phone", "email"))
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(AccountUpdateView, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if request.user.username != self.object.username:
            return HttpResponseRedirect(reverse('account-guest-view',
                args=(self.object.id,)))

        return super(AccountUpdateView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        return super(AccountUpdateView, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, 'Your profile was successfully updated')
        return reverse('account-update', args=(self.object.id,))


class AccountSettingsView(generic.UpdateView):
    """
    This view updates user's data
    """
    model = get_user_model()
    form_class = MySettingsForm
    template_name = "accounts/account_settings.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if request.user.username != self.object.username:
            return HttpResponseRedirect(reverse('account-guest-view',
                args=(self.object.id,)))

        return super(AccountSettingsView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, 'Settings were successfully updated')
        return reverse('account-settings', args=(self.object.id,))



class AccountMyTravelsView(generic.ListView):
    """
    List view of all travels the user ever posted
    """
    template_name = 'accounts/account_mytravels.html'
    model = Travel

    def get_queryset(self):
        return Travel.objects.filter(traveller_id=self.request.user.id)



class AccountMyParcelsView(generic.ListView):
    """
    List view of all requests the user ever posted
    """
    template_name = 'accounts/account_myparcels.html'
    model = Parcel

    def get_queryset(self):
        return Parcel.objects.filter(requester_id=self.request.user.id)        