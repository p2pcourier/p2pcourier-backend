from accounts import views
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required as log
import django.contrib.auth.views as auth_views



urlpatterns = [
    url(r'^view/(?P<pk>\d+)$', log(views.AccountGuestView.as_view()),
        name='account-guest-view'),
    url(r'^update/(?P<pk>\d+)$', log(views.AccountUpdateView.as_view()),
        name='account-update'),
    url(r'^settings/(?P<pk>\d+)$', log(views.AccountSettingsView.as_view()),
        name='account-settings'),
    url(r'^mytravels/(?P<pk>\d+)$', log(views.AccountMyTravelsView.as_view()),
        name='account-mytravels'),
    url(r'^myrequests/(?P<pk>\d+)$', log(views.AccountMyParcelsView.as_view()),
        name='account-myparcels'),
]

if not settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
