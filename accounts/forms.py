import re
import autocomplete_light.shortcuts as autocomplete_light

from django.contrib.auth import get_user_model
from django.forms import ModelForm, ModelChoiceField, ValidationError
from django.forms import TextInput, Select, ImageField, FileInput, Textarea
from cities_light.models import City
from django.utils.translation import ugettext_lazy as _
from PIL import Image
from StringIO import StringIO


class MyUserForm(autocomplete_light.ModelForm):

    profile_pic = ImageField(required=False, widget=FileInput)

    class Meta:
        model = get_user_model()
        fields = (
            'first_name',
            'last_name', 
            'email',
            'dob',
            'country',
            'city',
            'phone',
            'about',
            'profile_pic',
        )

        help_texts = {
            'first_name': (_('Use no more than 50 symbols')),
            'last_name': (_('Use no more than 50 symbols')),
            'email': (_('Confirmation email will be sent to the'
                        ' provided email address')),
            'dob': (_('Enter your date of birth')),
            'country': (_('Country where you live')),
            'city': (_('City where you currently live')),
            'phone': (_('Note all symbols except digits, space and'
                       ' ()+- will be ignored')),
            'about': (_('Short description about yourself')),
        }
        
        widgets = {
            'first_name' : TextInput(attrs={
                                        'size': 50,
                                        'class': 'form-control',
                                    }),

            'last_name' : TextInput(attrs={
                                        'size': 50,
                                        'class': 'form-control',
                                    }),

            'email' : TextInput(attrs={
                                    'size': 50,
                                    'class': 'form-control',
                                }),

            'dob' : TextInput(attrs={
                                'data-provide': 'datepicker',
                                'class': 'form-control',
                                'data-date-autoclose': 'true',
                                'data-date-format': 'yyyy-mm-dd',
                                'placeholder': 'Date of birth',
                              }),

            'country': autocomplete_light.ChoiceWidget('CountryAutocomplete',
                            attrs={
                                'placeholder': 'Type your country name',
                                'class': 'form-control',
                                'style': 'display:block;',
                            }),

            'city': autocomplete_light.ChoiceWidget('CityUserAutocomplete',
                            attrs={
                                'placeholder': 'Type your city name',                            
                                'class': 'form-control',
                                'style': 'display:block;',
                            }),

            'phone': TextInput(attrs={
                                    'size': 50,
                                    'class': 'form-control',
                                }),
            'about': Textarea(attrs={'class': 'form-control'}),
        }


    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not re.match("^.+@.+\..+$", email):
            raise ValidationError(_('Please use valid email address'))
        return email

    def clean_phone(self):
        raw_phone = self.cleaned_data.get('phone')
        phone = re.findall(r'[\d\+()\-\s]', raw_phone)
        result = []
        if phone != []:
            if phone[0] == '+':
                result.append('+')
        result += [i for i in phone if i.isdigit()]
        return ''.join(result)

    def clean_profile_pic(self):
        """
        We need NOT to enforce the file size for the avatar. 300x300 seems to
        be a reasonable size, so we can just be nice and compress the avatars.
        """
        avatar = self.cleaned_data['profile_pic']
        if avatar:
            image = Image.open(StringIO(avatar.read()))
            image.thumbnail((300, 300), Image.ANTIALIAS)
            image_file = StringIO()
            image.save(image_file, 'png')
            avatar.file = image_file
        return avatar
    

class MySettingsForm(ModelForm):
    """
    For to update the additional settings via settings section in user profile
    """
    class Meta:
        model = get_user_model()
        fields = (
            'receive_emails',
            'match_emails',
        )

        help_texts = {
            'receive_emails': (_('Please tick this box if you would like to '
                'get e-mail notifications about new messages')),
            'match_emails': (_('Please tick this box if you would like to '
                'get e-mail notifications about matching requests or travels'))
        }