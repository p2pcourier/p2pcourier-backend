from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.validators import MaxLengthValidator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
import os


def avatars_folder(instance, filename):
    return 'user_{0}/avatar.png'.format(str(instance.id))


@python_2_unicode_compatible
class MyUser(AbstractUser):
    """
    This is a User class which is fully customizable and
    based off of the AbstractUser from auth.models
    """
    dob = models.DateField("Date of birth", blank=True, null=True)
    phone = models.CharField("Phone number", max_length = 50,
                                    blank=True, null=True)
    country = models.ForeignKey('cities_light.Country', blank=True,
                                    null=True, verbose_name = "Home country")
    city = models.ForeignKey('cities_light.City', blank=True, null=True,
                                verbose_name = "Home city")
    profile_pic = models.ImageField(blank=True, null=True, 
                                upload_to=avatars_folder)
    receive_emails = models.BooleanField(("Receive e-mail notifications about "
        "new messages"), default=True)
    match_emails = models.BooleanField(("Receive e-mail notifications about "
        "matching travels or requests"), default=True)
    about = models.TextField("About me", blank=True,
        validators=[MaxLengthValidator(500)])

    def __str__(self):
        if self.first_name and self.last_name:
            name = u"{} {}".format(self.first_name, self.last_name)
        elif self.first_name:
            name = u"{}".format(self.first_name)
        else:
            name = u"{}".format(self.username)
        return name

    def get_absolute_url(self):
        """
        Gets the full url of the user to personal page
        """
        return reverse('account-update', args=[str(self.id)])
